package marsel.oky.myapplication

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*

class FragmentNilai : Fragment(), View.OnClickListener {

    // Inisiasi Variabel
    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var adapterMhs : SimpleCursorAdapter
    lateinit var adapterMatkul : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    lateinit var db : SQLiteDatabase
    var arrMhs = ArrayList<String>()
    var arrMatkul = ArrayList<String>()
    var namaMhs : String = ""
    var namaMatkul : String = ""
    var id_nilai : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_nilai,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.spMhs.onItemSelectedListener = MhsSelect
        v.spMatkul.onItemSelectedListener = MatkulSelect
        v.btnDeleteNilai.setOnClickListener(this)
        v.btnInsertNilai.setOnClickListener(this)
        v.btnUpdateNilai.setOnClickListener(this)
        v.lsNilai.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
        showDataMatkul()
        showDataNilai()
    }

    val MhsSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMhs.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = adapterMhs.getItem(position) as Cursor
            namaMhs = c.getString(c.getColumnIndex("_id"))
        }
    }

    val MatkulSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMatkul.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = adapterMatkul.getItem(position) as Cursor
            namaMatkul = c.getString(c.getColumnIndex("_id"))
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_nilai = c.getInt(c.getColumnIndex("_id"))
        namaMhs = c.getString(c.getColumnIndex("namaMhs"))
        namaMatkul = c.getString(c.getColumnIndex("namaMk"))
        v.edNilai.setText(c.getString(c.getColumnIndex("nilai")))
        v.spMhs.setSelection(getIndex(v.spMhs,namaMhs,arrMhs))
        v.spMatkul.setSelection(getIndex(v.spMatkul,namaMatkul,arrMatkul))
    }

    fun getIndex(spinner: Spinner, myString: String, arrayL: ArrayList<String>): Int {
        var a = spinner.count
        Log.d("Output","Jumlah item dalam array = $a")
        var b : String = ""
        for (i in 0 until a) {
            b = arrayL.get(i)
            Log.d("Output","Index ke $i = $b")
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataNilai(){
        var sql = "select n.id_nilai as _id, m.nama as namaMhs, k.nama_matkul as namaMk, n.nilai from nilai n, mhs m , matkul k " +
                "where n.nim=m.nim and n.kdmatkul=k.kdmatkul "
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_nilai,c,
            arrayOf("_id","namaMhs","namaMk","nilai"), intArrayOf(R.id.txidNilai,R.id.txNmMhs,R.id.txNmMatkul,R.id.txNilai),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsNilai.adapter = lsAdapter
    }

    fun showDataMhs(){
        val c : Cursor = db.rawQuery("select nama as _id from mhs order by nama asc",null)
        adapterMhs = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterMhs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMhs.adapter = adapterMhs
        v.spMhs.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrMhs.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun showDataMatkul(){
        val c : Cursor = db.rawQuery("select nama_matkul as _id from matkul order by nama_matkul asc",null)
        adapterMatkul = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterMatkul.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMatkul.adapter = adapterMatkul
        v.spMatkul.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrMatkul.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun insertDataNilai(nim : String, kdmatkul: String, nilai : String){
        var sql = "insert into nilai(nim, kdmatkul, nilai) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,kdmatkul,nilai))
        showDataNilai()
    }
    fun updateDataNilai(nim : String, kdmatkul: String, nilai : String,id_nilai : Int){
        var cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        cv.put("kdmatkul",kdmatkul)
        cv.put("nilai",nilai)
        db.update("nilai",cv,"id_nilai = $id_nilai",null)
        showDataNilai()
    }
    fun deleteDataNilai(id_nilai: Int){
        db.delete("nilai","id_nilai = $id_nilai",null)
        showDataNilai()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlMhs = "select nim from mhs where nama = '$namaMhs'"
        var sqlMk = "select kdmatkul from matkul where nama_matkul = '$namaMatkul'"
        val c : Cursor = db.rawQuery(sqlMhs,null)
        val c1 : Cursor = db.rawQuery(sqlMk,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataNilai(
                c.getString(c.getColumnIndex("nim")),
                c1.getString(c1.getColumnIndex("kdmatkul")),
                v.edNilai.text.toString()
            )
            v.spMhs.setSelection(0)
            v.spMatkul.setSelection(0)
            v.edNilai.setText("")
        }
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlMhs = "select nim from mhs where nama = '$namaMhs'"
        var sqlMk = "select kdmatkul from matkul where nama_matkul = '$namaMatkul'"
        val c : Cursor = db.rawQuery(sqlMhs,null)
        val c1 : Cursor = db.rawQuery(sqlMk,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            updateDataNilai(
                c.getString(c.getColumnIndex("nim")),
                c1.getString(c1.getColumnIndex("kdmatkul")),
                v.edNilai.text.toString(),
                id_nilai
            )
            v.spMhs.setSelection(0)
            v.spMatkul.setSelection(0)
            v.edNilai.setText("")
        }
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataNilai(id_nilai)
        v.spMhs.setSelection(0)
        v.spMatkul.setSelection(0)
        v.edNilai.setText("")
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnDeleteNilai -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Yakin akan menghapus data ini?")
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnInsertNilai -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnUpdateNilai -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }
}