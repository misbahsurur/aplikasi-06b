package marsel.oky.myapplication
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.view.*
import kotlinx.android.synthetic.main.frag_data_mhs.*
import kotlinx.android.synthetic.main.frag_data_mhs.view.*


class FragmentMatkul : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMatkul->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateMatkul->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsertMatkul ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()

            }
//            R.id.btnCari ->{
//                showDataMatkul(edNamaMatkul.text.toString())
//            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaProdi : String=""
    var arrProdi = ArrayList<String>()
    //   var idMhs : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMatkul.setOnClickListener(this)
        v.btnInsertMatkul.setOnClickListener(this)
        v.btnUpdateMatkul.setOnClickListener(this)
        v.spNamaProdi.onItemSelectedListener = this
//        v.btnCari.setOnClickListener(this)
        v.lsMatkul.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMatkul()
        showDataProdi()
    }

//    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
//        val c: Cursor = parent.adapter.getItem(position) as Cursor
//        v.edNimMhs.setText(c.getColumnIndex("nim"))
//        v.edNamaMhs.setText(c.getColumnIndex("nama"))
//        v.spinner.getItemAtPosition(c.getColumnIndex("nama_prodi"))
//    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.edKodeMatkul.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaMatkul.setText(c.getString(c.getColumnIndex("nama_matkul")))
        namaProdi = c.getString(c.getColumnIndex("nama_prodi"))
        v.spNamaProdi.setSelection(getIndex(v.spNamaProdi,namaProdi))
    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a) {
            b = arrProdi.get(i)
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataMatkul(){
        var sql = "select m.kdmatkul as _id, m.nama_matkul, p.nama_prodi from matkul m, prodi p " +
                "where m.id_prodi = p.id_prodi order by m.nama_matkul asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_matkul,c,
            arrayOf("_id","nama_matkul","nama_prodi"), intArrayOf(R.id.txIdMatkul,R.id.txNamaMatkul, R.id.txNamaProdiMatkul),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMatkul.adapter = lsAdapter
    }

    fun showDataProdi(){
        val c : Cursor = db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spNamaProdi.adapter = spAdapter
        v.spNamaProdi.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrProdi.add(temp) //add the item
            c.moveToNext()
        }
    }
    fun insertDataMatkul(kdmatkul : String , namaMataKuliah: String, id_prodi : Int){
        var sql = "insert into matkul (kdmatkul,nama_matkul,id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(kdmatkul,namaMataKuliah,id_prodi))
        showDataMatkul()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi ='$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataMatkul(v.edKodeMatkul.text.toString(), v.edNamaMatkul.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edKodeMatkul.setText("")
            v.edNamaMatkul.setText("")
            v.spNamaProdi.setSelection(0)
        }
    }

    fun updateDataMatkul(kdmatkul : String , namaMataKuliah: String, id_prodi : Int){
        var cv : ContentValues = ContentValues()
        cv.put("nama_matkul",namaMataKuliah)
        cv.put("id_prodi",id_prodi)
        db.update("matkul",cv,"kdmatkul = '$kdmatkul'",null)
        showDataMatkul()
    }
    val btnUpdateDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi ='$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            updateDataMatkul(v.edKodeMatkul.text.toString(), v.edNamaMatkul.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edKodeMatkul.setText("")
            v.edNamaMatkul.setText("")
            v.spNamaProdi.setSelection(0)
        }
    }
    fun deleteDataMatkul(kdmatkul: String){
        db.delete("matkul","kdmatkul = '$kdmatkul'",null)
        showDataMatkul()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMatkul(v.edKodeMatkul.text.toString())
        v.edKodeMatkul.setText("")
        v.edNamaMatkul.setText("")
        v.spNamaProdi.setSelection(0)
    }



}


